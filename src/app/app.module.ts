import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SignificantDigitsPipe } from './pipes/SignificantDigits.pipe';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormComponent } from './form/form.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatCardModule } from '@angular/material/card';
import { MatCommonModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { FormsModule } from '@angular/forms';
import { TreeComponent } from './tree/tree.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MathjaxModule } from 'mathjax-angular';
import { BmjComponent } from './bmj/bmj.component';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    TreeComponent,
    BmjComponent,
    SignificantDigitsPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatCardModule,
    MatCommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatGridListModule,
    FormsModule,
    MatExpansionModule,
    MathjaxModule.forRoot(/*Optional Config*/)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
