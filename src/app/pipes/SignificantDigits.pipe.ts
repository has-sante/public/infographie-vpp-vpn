import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'SignificantDigits'})
export class SignificantDigitsPipe implements PipeTransform {
  transform(nb: number): string {
    if(nb >=10 && nb <=90) {
        return Math.round(nb+Number.EPSILON).toString();
       } else if ((nb >=1 && nb <10) || (nb > 90 && nb <=99)) {
        return nb.toFixed(1);
       } else if ((nb >=0.01 && nb <1) || (nb > 99 && nb <= 99.99)) {
        return nb.toFixed(2);
       } else if (nb < 0.01) {
         return "<0,01";
       } else if (nb > 99.99) {
        return ">99.99";
      }
       return "Z";
  }
}