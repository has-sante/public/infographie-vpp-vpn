import { Component, Input, OnChanges, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { ViewEncapsulation } from '@angular/core';
import { thresholdFreedmanDiaconis } from 'd3';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TreeComponent implements OnInit, OnChanges {

  @Input() vpp!: number;
  @Input() prevalence!: number;
  @Input() sensibilite!: number;
  @Input() specificite!: number;
  @Input() nbPersonnes!: number;
  @Input() vraiPositifs: number;
	@Input() fauxPositifs: number;
	@Input() vraiNegatifs: number;
	@Input() fauxNegatifs: number;
  @Input() vraiPositifsDisplay:string;
	@Input() fauxPositifsDisplay:string;
	@Input() vraiNegatifsDisplay:string;
	@Input() fauxNegatifsDisplay:string;
  rayon: number = 18;
  maladesLabel:string;
  nonMaladesLabel:string;
  testPositifsVraisPLabel:string;
  testPositifsFauxPLabel:string;
  testNegatifsVraisNLabel:string;
  testNegatifsFauxNLabel:string;
  vraiPositifsLabel:string;
  fauxNegatifsLabel:string;
  vraiNegatifsLabel:string;
  fauxPositifsLabel:string;

  constructor() {
  }

  private treeData: any;
  private svg: any;
  private margin: number = 50;
  private width: number = 800 - (this.margin * 2);
  private height: number = 400 - (this.margin * 2);

  ngOnInit(): void {

  }

  ngOnChanges(): void {
    if (this.prevalence != null && this.sensibilite != null && this.specificite != null) {
      this.createSvg()
      this.createTreeData();
      this.drawTree(this.treeData)
    }
  }

  private createTreeData(): void {
    //compute singulier pluriel
    (this.vraiPositifs + this.fauxNegatifs) > 1 ? this.maladesLabel = " malades" : this.maladesLabel = " malade";
    (this.vraiNegatifs + this.fauxPositifs) > 1 ? this.nonMaladesLabel = " non-malades" : this.maladesLabel = " non-malade";
    if(this.vraiPositifs > 1) {
      this.testPositifsVraisPLabel = " tests positifs";
      this.vraiPositifsLabel = "Vrais positifs";
    } else {
      this.testPositifsVraisPLabel = " test positif";
      this.vraiPositifsLabel = "Vrai positif";
    }
    if(this.vraiNegatifs > 1) {
      this.testNegatifsVraisNLabel = " tests négatifs";
      this.vraiNegatifsLabel = "Vrais négatifs";
    } else {
      this.testNegatifsVraisNLabel = " test négatif";
      this.vraiNegatifsLabel = "Vrai négatif";
    }
    if(this.fauxPositifs > 1) {
      this.testPositifsFauxPLabel = " tests positifs";
      this.fauxPositifsLabel = "Faux positifs";
    } else {
      this.testPositifsFauxPLabel = " test positif";
      this.fauxPositifsLabel = "Faux positif";
    }
    if(this.fauxNegatifs > 1) {
      this.testNegatifsFauxNLabel = " tests négatifs";
      this.fauxNegatifsLabel = "Faux négatifs";
    } else {
      this.testNegatifsFauxNLabel = " test négatif";
      this.fauxNegatifsLabel = "Faux négatif";
    }
    this.treeData = {
      "name": "Population : " + this.nbPersonnes + " individus",
      "value": this.nbPersonnes,
      "children": [
        {
          "name": (this.vraiPositifs+this.fauxNegatifs)+ this.maladesLabel,
          "value": this.vraiPositifs+this.fauxNegatifs,
          "linked": "Prévalence = " + this.prevalence + "%",
          "anchor": "end",
          "color": "#7c8696",
          "children": [
            {
              "name": this.vraiPositifsDisplay + this.testPositifsVraisPLabel,
              "color": "#FF6168",
              "anchor": "end",
              "linked": "Sensibilité = " + this.sensibilite + "%",
              "value": this.vraiPositifs,
              "label" : this.vraiPositifsLabel,
              "colorLabel" : "#FF6168"
            },
            {
              "name": this.fauxNegatifsDisplay+ this.testNegatifsFauxNLabel,
              "color": "#fc9303",
              "value": this.fauxNegatifs,
              "label" : this.fauxNegatifsLabel,
              "colorLabel" : "#fc9303"
            }
          ]
        },
        {
          "name": this.fauxPositifs+this.vraiNegatifs+this.nonMaladesLabel,
          "value": this.fauxPositifs+this.vraiNegatifs,
          "color": "#7c8696",
          "children": [
            {
              "name": this.fauxPositifsDisplay + this.testPositifsFauxPLabel,
              "color": "#fc9303",
              "value": this.fauxPositifs,
              "label" : this.fauxPositifsLabel,
              "colorLabel" : "#fc9303"
            },
            {
              "name": this.vraiNegatifsDisplay + this.testNegatifsVraisNLabel,
              "anchor": "start",
              "color": "#40BFC1",
              "linked": "Spécificité = " + this.specificite + "%",
              "value":  this.vraiNegatifs,
              "label" : this.vraiNegatifsLabel,
              "colorLabel" : "#40BFC1"
            }
          ]
        }
      ]
    };
  }

  private createSvg(): void {
    d3.select("#tree").select("svg").remove();
    this.svg = d3.select("#tree")
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + this.margin + ")");

    this.svg.append("rect")
      .attr("x",0)
      .attr("y", -this.margin)
      .attr("height", this.height + (this.margin*1.8))
      .attr("width", this.width + (this.margin))
      .attr("fill", "#F4F3F3")
      .attr("rx", "15px")
      .attr("ry", "15px")

  }

  private drawTree(data: any) {
    var treemap = d3.tree().size([this.width+this.margin, this.height-this.margin/2]);
    var nodes = d3.hierarchy(data);
    //console.log(nodes)
    // maps the node data to the tree layout
    nodes = treemap(nodes);
    var sqrtScale = d3.scaleSqrt()
    .domain([0, this.nbPersonnes])
    .range([4, 25]);
    // append the svg obgect to the body of the page
    // appends a 'group' element to 'svg'
    // moves the 'group' element to the top left margin

    // adds the links between the nodes
    var link = this.svg.selectAll(".link")
      .data(nodes.descendants().slice(1))
      .enter()

    let that = this;
    link.append("path")
      .attr("class", "link")
      .attr("d", function (d: any) {
        return "M" + d.x + "," + d.y
          + "C" + d.x + "," + (d.y + d.parent.y) / 2
          + " " + d.parent.x + "," + (d.y + d.parent.y) / 2
          + " " + d.parent.x + "," + d.parent.y;
      })
      .style("stroke", function (d: any) {
        return d.data.color;
      })
      .style("stroke-width", function (d: any) {
        return 1+10*d.data.value/that.nbPersonnes;
      })
      .style("stroke-dasharray", function (d: any) {
        if(!d.data.name.includes(that.maladesLabel) && !d.data.name.includes(that.nonMaladesLabel)) {
            return d.data.value <= 1 ? 5.5 : 0;
        }
        return 0;
      });

    link.append("text")
      .attr("font-family", "Raleway")
      .attr("font-size", "16px")
      .style('fill', function (d: any) {
        return d.data.color
      })
      .attr("font-weight", "bold")
      .attr("transform", function (d: any) {
        let marginToPath = 2;
        if (d.data.anchor === "end") {
          marginToPath = 2.35;
        } else if (d.data.anchor === "start") {
          marginToPath = 1.95;
        }
        return "translate(" +
          ((d.parent.x + d.x) / marginToPath) + " " +
          ((d.parent.y + d.y) / 2) + ")";
      })
      .attr("dy", ".35em")
      .attr("text-anchor", function (d: any) {
        return d.data.anchor;
      })
      .text(function (d: any) {
        return d.data.linked;
      });

    var g = this.svg.append("g");

    // adds each node as a group
    var node = g.selectAll(".node")
      .data(nodes.descendants())
      .enter().append("g")
      .attr("class", function (d: any) {
        return "node" +
          (d.children ? " node--internal" : " node--leaf");
      })
      .attr("transform", function (d: any) {
        return "translate(" + d.x + "," + d.y + ")";
      });
    node
      .append("circle")
      .attr("fill", function(d:any) {
        if(d.data.name.includes(that.maladesLabel) || d.data.label?.includes(that.vraiPositifsLabel)) {
          return "#FF6168"
        } else if(d.data.name.includes(that.nonMaladesLabel) || d.data.label?.includes(that.vraiNegatifsLabel)) {
          return "#40BFC1"
        } else if( d.data.label?.includes(that.fauxNegatifsLabel) || d.data.label?.includes(that.fauxPositifsLabel)) {
          return "#fc9303"
        } else {
          return "#5E7CEA";
        }
      })
      .attr("r", function (d: any) {
        return sqrtScale(d.data.value)
      })
      .on("mouseover", function (event: any, d: any) {
        //Tooltip
        //  .style("opacity", 1)
        //  .style("stroke", "black")
      })
      .on("mousemove", function (event: any, d: any) {
        //Tooltip
        //  .html("Nombre de patients : " + d.data.value)
        //  .style("left", event.layerX + 70 + "px")
        //  .style("top", event.layerY + 110 + "px")
        //  .style('display', 'block')
      })
      .on("mouseleave", function (event: any, d: any) {
        //Tooltip
        //  .style("opacity", 0)
        //  .style("stroke", "none")
      });

    node.append("text")
      .text(function(d:any) {
        if(d.data.label?.includes(that.vraiPositifsLabel) || d.data.label?.includes(that.fauxPositifsLabel)) {
          return "+"
        } else if(d.data.label?.includes(that.fauxNegatifsLabel) || d.data.label?.includes(that.vraiNegatifsLabel)) {
          return "-"
        }
        return null;
      })
      .attr("fill","white")
      .attr("font-weight",750)
      .attr("font-size","16px")
      .attr("font-family","Arial")
      .attr("text-anchor","middle")
      .attr("x", function (d:any) {
        if(d.data.label?.includes(that.vraiPositifsLabel) || d.data.label?.includes(that.fauxPositifsLabel)) {
          return 0;
        } else if(d.data.label?.includes(that.fauxNegatifsLabel) || d.data.label?.includes(that.vraiNegatifsLabel)) {
          return -0.5;
        }
        return 0;
      })
      .attr("y", 5.5)


    // adds the text to the node
    node.append("text")
      .attr("font-family", "Raleway")
      .attr("font-size", "15px")
      .attr("dy", ".35em")
      .attr("x", function(d:any) {
        if(d.data.name.includes(that.maladesLabel)) {
          return -20 - that.rayon * (d.data.value / that.nbPersonnes);
        } else if (d.data.name.includes(that.nonMaladesLabel)) {
          return 20 + that.rayon * (d.data.value / that.nbPersonnes);
        } else {
          return 0;
        }
      })
      .attr("y", function (d: any) {  console.log(d.data.name)
        if(d.data.name.includes(that.maladesLabel) || d.data.name.includes(that.nonMaladesLabel)) {
          return 0;
        } else if (d.data.name.includes("Population")) {
          return -37;
        } else {
          return 37;
          return d.children ? -20 - that.rayon * (d.data.value / that.nbPersonnes) : 20 + that.rayon * (d.data.value / that.nbPersonnes);
        }
      })
      .style("text-anchor", function(d:any) {
        if(d.data.name.includes(that.maladesLabel)) {
          return "end";
        } else if (d.data.name.includes(that.nonMaladesLabel)) {
          return "start";
        } else {
          return "middle";
        }
      })
      .text(function (d: any) { return d.data.name; });

    node.append("text")
      .attr("font-family", "Raleway")
      .attr("font-weight", "bold")
      .attr("dy", ".35em")
      .attr("y", function (d: any) { return d.children ? -35 - that.rayon * (d.data.value / that.nbPersonnes) : 52 })
      .style("text-anchor", "middle")
      .style("fill", function (d: any) { return d.data.colorLabel; })
      .text(function (d: any) { return d.data.label; });

  }

}

