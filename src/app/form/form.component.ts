import { Component, Input, OnInit } from '@angular/core';
import { SignificantDigitsPipe } from '../pipes/SignificantDigits.pipe';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {

  @Input() prevalence: number = 20;
  @Input() specificite: number = 90;
  @Input() sensibilite: number = 80;
  showTrueVpp: boolean = false;
  vpp:number;
  vpn:number;
  nbPersonnes:number = 100;
  maladieVpp!: number;
  panelOpenState = false;
  formuleVpp !: string;
  formuleVpn !: string;
  nbPositifs: number;
  nbNegatifs: number;
  vraiPositifs: number;
  fauxPositifs: number;
  vraiNegatifs: number;
  fauxNegatifs: number;
  vraiPositifsDisplay:string;
	fauxPositifsDisplay:string;
	vraiNegatifsDisplay:string;
	fauxNegatifsDisplay:string;

  constructor() { 
  }


  ngOnInit(): void {
    this.computeAllValues();
    this.displayFormules();
  }

  formatLabel(value: number) {
    if(value == 100) {
      return value+"%"
    }
    return value+" %";
  }

  sensibilityChanges(event:any):void{
    this.dataChanged(event);
  }

  specificityChanges(event:any):void{
    this.dataChanged(event);
  }

  prevalenceChanges(event:any):void{
    this.dataChanged(event);
  }

  dataChanged(event:any) {
    this.computeAllValues()
    this.displayFormules();
  }

  displayFormules():void {
    this.formuleVpp = "$VPP = \\frac {VP} {VP + FP} = \\frac{{Pr} ~ * ~ {Se}} {{Pr} * {Se} ~ + ~ (1 - Pr) * (1 - Sp)}\\simeq " + this.getSignificantDigits(this.vpp) + "\\% $"
    this.formuleVpn = "$VPN = \\frac {VN} {VN + FN} = \\frac{{(1 - Pr) ~ * ~ Sp}} {{(1 - Pr)} * Sp ~ + ~ Pr * (1 - Se)}\\simeq " + this.getSignificantDigits(this.vpn)+"\\% $"
  }

  checkVppSlider(): void {
    this.showTrueVpp = true;
  }

  computeVPP(): void{
    const vppBrut:number = 100*((this.prevalence * this.sensibilite) / (this.prevalence * this.sensibilite + ((100 - this.prevalence) * (100 - this.specificite))));
    //this.vpp = Math.round(100*(vppBrut+Number.EPSILON))/100;
    this.vpp = vppBrut;
  }

  computeVPN(): void {
    const vpnBrut:number = 100*(this.specificite*(100-this.prevalence))/(this.specificite*(100-this.prevalence)+(100-this.sensibilite)*this.prevalence);
    //this.vpn = Math.round((1e2*(vpnBrut+Number.EPSILON)))/1e2;
    this.vpn = vpnBrut
  }

  computeEffectifs(){
    this.nbPositifs = Math.floor(this.nbPersonnes*(this.prevalence/100)+Number.EPSILON);
    this.nbNegatifs = this.nbPersonnes - this.nbPositifs;
    this.vraiPositifs = Math.floor(this.nbPositifs*(this.sensibilite/100)+Number.EPSILON);
    this.fauxNegatifs = this.nbPositifs - this.vraiPositifs;
    this.vraiNegatifs = Math.floor(this.nbNegatifs*(this.specificite/100)+Number.EPSILON);
    this.fauxPositifs = this.nbNegatifs - this.vraiNegatifs;
  }

  computeAllValues(){
    this.computeEffectifs();
    this.computeVPP();
    this.computeVPN();
    this.getEffectifDisplay();
  }

  getEffectifDisplay() {
    this.vraiNegatifsDisplay = this.applyBornes(this.vraiNegatifs);
    this.fauxNegatifsDisplay = this.applyBornes(this.fauxNegatifs);
    this.vraiPositifsDisplay = this.applyBornes(this.vraiPositifs);
    this.fauxPositifsDisplay = this.applyBornes(this.fauxPositifs);
  }
  
  applyBornes(nb:number) {
    if(nb > (this.nbPersonnes-1)) {
      return ">"+(nb-1);
    }
    if(nb < 1) {
      return "<1";
    }
    return ""+nb;
  }

  truncate(nb:number, digits:number):number {
    return Math.trunc(nb*Math.pow(10, digits))/Math.pow(10, digits)
  }

  getSignificantDigits(nb:number):string {
     if(nb >=10 && nb <=90) {
      return Math.round(nb+Number.EPSILON).toString();
     } else if ((nb >=1 && nb <10) || (nb > 90 && nb <=99)) {
      return nb.toFixed(1);
     } else if ((nb >=0.01 && nb <1) || (nb > 99 && nb <= 99.99)) {
      return nb.toFixed(2);
     } else if (nb < 0.01) {
       return "<0,01";
     } else if (nb > 99.99) {
      return ">99.99";
    }
     return "Z";
  }

}
