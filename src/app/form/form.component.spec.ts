import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormComponent } from './form.component';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('match expecting values case 1', () => {
    component.specificite = 95;
    component.sensibilite = 91;
    component.prevalence = 2;
    component.computeVPP();
    component.computeVPN();
    expect(component.vpp).toEqual(17);
    expect(component.vpn).toEqual(99.78);
  })

  it('match expecting values case 2', () => {
    component.specificite = 70;
    component.sensibilite = 99;
    component.prevalence = 0.5;
    component.computeVPP();
    component.computeVPN();
    expect(component.vpp).toEqual(2.1);
    expect(component.vpn).toBeGreaterThanOrEqual(99.99);
  })
});
