import { Component, ElementRef, Input, Output, OnInit, ViewChild, ViewEncapsulation, EventEmitter, OnChanges } from '@angular/core';
import * as d3 from 'd3';

@Component({
	selector: 'app-bmj',
	templateUrl: './bmj.component.html',
	styleUrls: ['./bmj.component.css'],
	encapsulation: ViewEncapsulation.None
})
export class BmjComponent implements OnInit, OnChanges {


	constructor() { }

	@Input() vpp!: number;
	@Input() vpn!: number;
	@Input() prevalence!: number;
	@Output() prevalenceChange: EventEmitter<number> = new EventEmitter<number>();
	@Input() sensibilite!: number;
	@Output() sensibiliteChange: EventEmitter<number> = new EventEmitter<number>();
	@Input() specificite!: number;
	@Output() specificiteChange: EventEmitter<number> = new EventEmitter<number>();
	@Input() nbPersonnes!: number;
	@Input() vraiPositifs: number;
	@Input() fauxPositifs: number;
	@Input() vraiNegatifs: number;
	@Input() fauxNegatifs: number;
	@Input() vraiPositifsDisplay:string;
	@Input() fauxPositifsDisplay:string;
	@Input() vraiNegatifsDisplay:string;
	@Input() fauxNegatifsDisplay:string;
	preTestProbOkay = true;
	sensitivityOkay = true;
	specificityOkay = true;
	simulation: any;
	circlesData: any = [];
	circles: any;
	forceX: any;
	forceY: any;
	invertVpn: number
	marquePlurielVP:string;
	marquePlurielFP:string;
	marquePlurielVN:string;
	marquePlurielFN:string;
	collisionForce = d3.forceCollide(6.5)
		.strength(1)
		.iterations(10);

	ngOnInit(): void {
		for (var i = 0; i < this.vraiPositifs; i++) {
			this.circlesData.push({ "kind": "1" });
		}
		for (var i = 0; i < this.fauxNegatifs; i++) {
			this.circlesData.push({ "kind": "2" });
		}
		for (var i = 0; i < this.fauxPositifs; i++) {
			this.circlesData.push({ "kind": "3" });
		}
		for (var i = 0; i < this.vraiNegatifs; i++) {
			this.circlesData.push({ "kind": "4" });
		}
		this.circles = d3.select("#viz").selectAll("svg").append("g").selectAll("g")
			.data(this.circlesData)
			.enter()
			.append("g")

		this.circles.append("circle")
			.attr("class", "circle")
			.attr("r", 6)
			.attr("cx", 352.597)
			.attr("cy", 469.667)
			.attr("fill", function (d: any) {
				if (d.kind == "1") {
					return "#FF6168";
				} else if (d.kind == "2") {
					return "#fc9303";
				} else if (d.kind == "3") {
					return "#fc9303";
				} else {
					return "#40BFC1";
				}

			});

		this.circles.append("path")
			.attr("class", "symbol")
			.attr("fill", "#FFFFFF")
			.attr("d", function (d: any) {
				if (d.kind == "1" || d.kind == "3") {
					return "M220.04,330.169v-1.276c0-0.295,0.239-0.534,0.534-0.534h1.276c0.21,0,0.387-0.124,0.474-0.3c0.035-0.071,0.06-0.149,0.06-0.234v-1.148c0-0.085-0.024-0.163-0.06-0.234c-0.087-0.176-0.264-0.3-0.474-0.3h-1.276c-0.295,0-0.534-0.239-0.534-0.534v-1.276c0-0.295-0.239-0.534-0.534-0.534h-1.148c-0.295,0-0.534,0.239-0.534,0.534v1.276c0,0.295-0.239,0.534-0.534,0.534h-1.276c-0.217,0-0.402,0.131-0.486,0.317c-0.03,0.067-0.048,0.139-0.048,0.217v1.148c0,0.078,0.018,0.15,0.048,0.217c0.083,0.186,0.269,0.317,0.486,0.317h1.276c0.295,0,0.534,0.239,0.534,0.534v1.276c0,0.295,0.239,0.534,0.534,0.534h1.148C219.801,330.703,220.04,330.464,220.04,330.169z";
				} else {
					return "M528.464,326.144c-0.217,0-0.403,0.131-0.486,0.317c-0.03,0.067-0.048,0.139-0.048,0.217v1.148c0,0.078,0.018,0.15,0.048,0.217c0.083,0.186,0.269,0.317,0.486,0.317h5.835c0.21,0,0.387-0.124,0.474-0.3c0.035-0.071,0.06-0.149,0.06-0.234v-1.148c0-0.085-0.024-0.163-0.06-0.234c-0.087-0.176-0.265-0.3-0.474-0.3H528.464z";
				}

			})
			.attr("transform", function (d: any) {
				if (d.kind == "1") {
					return "translate(133.687, 142.433)";
				} else if (d.kind == "2") {
					return "translate(-178.762, 142.433)";
				} else if (d.kind == "3") {
					return "translate(133.687, 142.433)";
				} else if (d.kind == "4") {
					return "translate(-178.762, 142.433)";
				} else {
					return "translate(-500, -500)";
				}

			});

		this.forceX = d3.forceX(function (d: any) {
			if (d.kind == "1") {
				return -134.354;
			} else if (d.kind == "2") {
				return -134.354;
			} else if (d.kind == "3") {
				return 134.354;
			} else if (d.kind == "4") {
				return 134.354;
			} else {
				return -500;
			}
		});
		this.forceY = d3.forceY(function (d: any) {

			if (d.kind == "1") {
				return -175.337;
			} else if (d.kind == "2") {
				return 61.449;
			} else if (d.kind == "3") {
				return -175.337;
			} else if (d.kind == "4") {
				return 61.449;
			} else {
				return -500;
			}
		});

		var that = this;
		this.simulation = d3.forceSimulation()
			.nodes(this.circlesData)
			.alphaDecay(0.01)
			.velocityDecay(0.6)
			.force("forceX", this.forceX)
			.force("forceY", this.forceY)
			.force("collisionForce", this.collisionForce)
			.force("charge", d3.forceManyBody().strength(0.5))
			.on("tick", function () {
				that.circles.attr("transform", function (d: any) {
					return "matrix(1 0 0 1 " + d.x + " " + d.y + ")";
				});
			});
	}

	ngOnChanges(): void {
		this.invertVpn = 100 - this.vpn;
		this.updateGraphic();
		this.applyPluriel();
	}

	updateGraphic() {

		// update circle positions

		for (var i = 0; i < this.circlesData.length; i++) {
			if (i < this.vraiPositifs) {
				this.circlesData[i].kind = "1";
			} else if (i < this.vraiPositifs + this.fauxNegatifs) {
				this.circlesData[i].kind = "2";
			} else if (i < this.vraiPositifs + this.fauxNegatifs + this.fauxPositifs) {
				this.circlesData[i].kind = "3";
			} else {
				this.circlesData[i].kind = "4";
			}

		}

		var that = this;
		this.simulation = d3.forceSimulation()
			.nodes(this.circlesData)
			.alphaDecay(0.01)
			.velocityDecay(0.6)
			.force("forceX", this.forceX)
			.force("forceY", this.forceY)
			.force("collisionForce", this.collisionForce)
			.force("charge", d3.forceManyBody().strength(0.5))
			.on("tick", function () {
				that.circles.attr("transform", function (d: any) {
					return "matrix(1 0 0 1 " + d.x + " " + d.y + ")";
				});
			});
		if(this.circles === undefined) {
			return
		}
		this.circles.selectAll("circle")?.attr("fill", function (d: any) {
			if (d.kind == "1") {
				return "#FF6168";
			} else if (d.kind == "2") {
				return "#fc9303";
			} else if (d.kind == "3") {
				return "#fc9303";
			} else {
				return "#40BFC1";
			}
			
		});

		this.circles.select(".symbol")
			.attr("fill", "#FFFFFF")
			.attr("d", function (d: any) {
				if (d.kind == "1" || d.kind == "3") {
					return "M220.04,330.169v-1.276c0-0.295,0.239-0.534,0.534-0.534h1.276c0.21,0,0.387-0.124,0.474-0.3c0.035-0.071,0.06-0.149,0.06-0.234v-1.148c0-0.085-0.024-0.163-0.06-0.234c-0.087-0.176-0.264-0.3-0.474-0.3h-1.276c-0.295,0-0.534-0.239-0.534-0.534v-1.276c0-0.295-0.239-0.534-0.534-0.534h-1.148c-0.295,0-0.534,0.239-0.534,0.534v1.276c0,0.295-0.239,0.534-0.534,0.534h-1.276c-0.217,0-0.402,0.131-0.486,0.317c-0.03,0.067-0.048,0.139-0.048,0.217v1.148c0,0.078,0.018,0.15,0.048,0.217c0.083,0.186,0.269,0.317,0.486,0.317h1.276c0.295,0,0.534,0.239,0.534,0.534v1.276c0,0.295,0.239,0.534,0.534,0.534h1.148C219.801,330.703,220.04,330.464,220.04,330.169z";
				} else {
					return "M528.464,326.144c-0.217,0-0.403,0.131-0.486,0.317c-0.03,0.067-0.048,0.139-0.048,0.217v1.148c0,0.078,0.018,0.15,0.048,0.217c0.083,0.186,0.269,0.317,0.486,0.317h5.835c0.21,0,0.387-0.124,0.474-0.3c0.035-0.071,0.06-0.149,0.06-0.234v-1.148c0-0.085-0.024-0.163-0.06-0.234c-0.087-0.176-0.265-0.3-0.474-0.3H528.464z";
				}

			})
			.attr("transform", function (d: any) {
				if (d.kind == "1") {
					return "translate(133.687, 142.433)";
				} else if (d.kind == "2") {
					return "translate(-178.762, 142.433)";
				} else if (d.kind == "3") {
					return "translate(133.687, 142.433)";
				} else if (d.kind == "4") {
					return "translate(-178.762, 142.433)";
				} else {
					return "translate(-500, -500)";
				}

			});

	}

	checkNumber(zeNumber: number) {
		if (isNaN(zeNumber)) {
			return false;
		} else if (zeNumber == null) {
			return false;
		} else if (zeNumber > 100) {
			return false;
		} else if (zeNumber < 0) {
			return false;
		} else {
			return true;
		}

	}

	dataChanged(event: any) {
		if(!this.checkNumber(this.prevalence)){
			d3.select("#preTestProbError").attr("display", null);
			return;
		} else {
			d3.select("#preTestProbError").attr("display", "none");
		}
		if(!this.checkNumber(this.specificite)){
			d3.select("#specificityError").attr("display", null);
			return;
		} else {
			d3.select("#specificityError").attr("display", "none");
		}
		if(!this.checkNumber(this.sensibilite)){
			d3.select("#sensitivityError").attr("display", null);
			return;
		} else {
			d3.select("#sensitivityError").attr("display", "none");
		}
		this.prevalence = +this.prevalence.toFixed(2);
		this.specificite = +this.specificite.toFixed(2);
		this.sensibilite = +this.sensibilite.toFixed(2);
		this.prevalenceChange.emit(this.prevalence);
		this.specificiteChange.emit(this.specificite);
		this.sensibiliteChange.emit(this.sensibilite);
	}

	applyPluriel() {
		this.vraiPositifs > 1 ? this.marquePlurielVP = "s" : this.marquePlurielVP = "";
		this.vraiNegatifs > 1 ? this.marquePlurielVN = "s" : this.marquePlurielVN = "";
		this.fauxPositifs > 1 ? this.marquePlurielFP = "s" : this.marquePlurielFP = "";
		this.fauxNegatifs > 1 ? this.marquePlurielFN = "s" : this.marquePlurielFN = "";
	}

}
