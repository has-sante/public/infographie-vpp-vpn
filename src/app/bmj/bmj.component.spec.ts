import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BmjComponent } from './bmj.component';

describe('BmjComponent', () => {
  let component: BmjComponent;
  let fixture: ComponentFixture<BmjComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BmjComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BmjComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
